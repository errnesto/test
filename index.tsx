import React from 'react'
import ReactDOMServer from 'react-dom/server'
import sharp from 'sharp'

// set up koa + middleware
import Koa from 'koa'
import Router from 'koa-router'
import logger from 'koa-morgan'
import views from 'koa-views'

import { join } from 'path'

require('dotenv').config()

import {
  fixUrlPrefix,
  normalizeHost,
  isFacebookBot,
  isTwitterBot,
  routeUrl,
  imageUrl
} from './utils'

import LineChart, { getLine } from './components/LineChart/LineChart'
import MapChart, { getRegionalData } from './components/MapChart/MapChart'
import version from './version-middleware'

// configure environment variables
const {
  URL_PREFIX = '',
  PORT = '9000',
  PROTOCOL = 'https',
  ARTICLE_URL = 'https://rbb24.de'
} = process.env

const app = new Koa()
if (module.parent == null) app.use(logger('combined')) // don't log tests

// set up templating
app.use(views(join(__dirname, 'views'), { extension: 'pug' }))

// expose current git revision
app.use(version.header())

// set up router
const router = new Router({ prefix: fixUrlPrefix(URL_PREFIX) })

// serve thumbnails

router.get('/thumbs/:thumb', async ctx => {
  const filename = ctx.params.thumb
  const { query } = ctx

  // sanity check
  if (!filename.match(/\.png$/)) {
    await ctx.redirect(normalizeHost(ARTICLE_URL))
    return
  }

  switch (filename) {
    case 'over-time.png':
      const line = await getLine(query.name, query.gender)
      const lineChart = (
        <LineChart width={600} height={315} line={line} gender={query.gender} />
      )
      const lineSvg = Buffer.from(ReactDOMServer.renderToString(lineChart))
      const linePng = await sharp(lineSvg).png()
      ctx.type = 'image/png'
      ctx.body = linePng
      break

    case 'map.png':
      const districts = await getRegionalData(query.name, query.gender)
      const mapChart = (
        <MapChart regionalData={districts} gender={query.gender} />
      )
      const mapSvg = Buffer.from(ReactDOMServer.renderToString(mapChart))
      const mapPng = await sharp(mapSvg).png()
      ctx.type = 'image/png'
      ctx.body = mapPng
  }
})

// serve up the response for the facebook / twitter bot
router.get('/', async ctx => {
  // for the expected parameter structure look below
  const { query } = ctx

  // build the response for the requested location
  const sharingIdentifier = routeUrl(PROTOCOL, ctx)
  const userAgent = ctx.headers['user-agent']

  if (isFacebookBot(userAgent) || isTwitterBot(userAgent)) {
    const title =
      query.type == 'over-time'
        ? `So beliebt war der Name ${query.name} in den letzten Jahren.`
        : `So beliebt war der Name ${query.name} in den verschiedenen Bezierken.`

    const description =
      query.type == 'over-time'
        ? `Finde heraus wie beliebt dein Name war.`
        : `Finde heraus wo dein Name am beliebtesten ist.`

    const params = {
      sharingIdentifier: sharingIdentifier,
      imgUrl: imageUrl(
        PROTOCOL,
        ctx.headers,
        query.type,
        query.name,
        query.gender
      ),
      title: title,
      description: description
    }
    await ctx.render('sharing_tags', params)
  } else {
    await ctx.redirect(ARTICLE_URL)
  }
})

app.use(router.routes()).use(router.allowedMethods())

// start accepting requests
if (!module.parent) {
  const port = parseInt(PORT, 10)
  app.listen(port, _ => {
    console.log('Listening on port ' + port)
  })
}

module.exports = app
