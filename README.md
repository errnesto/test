# Berliner Vornamen - Sharing Proxy 

Diese Applikation bildet den Proxy, der das Sharing-Feature der Vornamen Visualisierungen bereitstellt. Sie liefert die entsprechenden Meta-Tags für Facebook und Twitter bzw. leitet Nicht-Bots auf den Artikel weiter.

Der Server erwartet Requests auf `?type=map?name=Manuel&gender=m`.

- **`type`**: `map` oder `over-time` – bestimmt welches Vorschaubild angezeigt wird
- **`name`**: Der Name für welchen Daten geladen werden
- **`gender`**: Das Geschlecht muss zusätzlich angegben werden, da es Namen gibt die für zwei Gelschlechter vergeben werden.

## Technische Details

Die App basiert auf `nodejs` und typescript

```bash
# Dependencies installieren
npm install
# Server bauen
npm run build
# Server starten
npm start
# Dev server starten (spart einem den build)
npm run dev

# Tests ausführen
npm test
# Tests kontinuierlich ausführen
npm test -- --watch
# Testabdeckung überprüfen
npm run coverage
```

### Deployment

Es liegt ein `Dockerfile` bei, das mit folgenden Argumenten gebaut werden kann:

```
$ docker build --tag=$TAG .
```

`docker run $TAG` Das startet den Server auf Port 9000.

Wenn man die Tests im Docker-Container ausführen möchte, muss die `NODE_ENV` entsprechend gesetzt werden:
```
$ docker build --tag=$TAG --build-arg NODE_ENV=development .
$ docker run $TAG npm test
```

### Umgebungsvariablen

- `NODE_ENV` ist entweder `development` oder `production`
- `PORT` bestimmt den Port, auf dem HTTP-Anfragen eingehen. Default: `9000`
- `PROTOCOL` gibt an, ob der Server über `http` oder `https` angesprochen wird. Default: `https`
- `URL_PREFIX` muss gesetzt werden, wenn der Server nicht unter `/` eingehängt wird. Default: `''` 
- `ARTICLE_URL` ist die URL der Artikels. Default: `https://www.rbb24.de`

### Version middleware

Der Server gibt für jede Response einen Header mit `X-Version` zurück. Mithilfe des untenliegenden Scriptes enthält dieser Header den branch und den aktuellen commit, womit sich nachvollziehen lässt welche Version genau deployt wurde. Das ganze sollte unter `.git/hooks/pre-commit` als Pre-Commit-Hook abgelegt werden und mithilfe von `chmod +x .git/hooks/pre-commit` ausführbar gemacht werden:

``` bash
#!/usr/bin/env bash

##
# make sure to put a .version file into the root before pushing to the server
##
if ! [ -x "$(command -v jq)" ]; then
	echo >&2 "jq executable not found. Skipping version bump."
	return 0
fi

package_json="$(git rev-parse --show-toplevel)/package.json"
major_version=$(jq ".version" $package_json | grep -oE "\d+" | head -n1)
version="$major_version.0.$(git rev-list --all --count)"
jq '. + {"version": "'"$version"'"}' "$package_json" > "$package_json.tmp"
rm "$package_json" && mv "$package_json.tmp" "$package_json"
git add "$package_json"
```
