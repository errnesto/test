// works nicely together with a pre-commit hook
const pkg = require('./package.json')

/**
 * Adds an http header containing the version string
 * @param {String} key Which header key to use, defaults to 'X-Version'
 * @return {Function} koa middleware
 */
function header (key = 'X-Version') {
  return async function versionHeader (ctx, next) {
    ctx.set(key, pkg.revision || pkg.version)
    await next()
  }
}

/**
 * Writes the version string into the http body
 * @return {Function} koa middleware
 */
function body () {
  return async function versionBody (ctx, next) {
    ctx.body = pkg.revision || pkg.version
    await next()
  }
}

export default { header, body }
