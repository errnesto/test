FROM node:13-alpine as builder

# install build tools so we can buil local npm packages
RUN apk add --no-cache make gcc g++ python

# create app directory
# RUN mkdir -p /usr/src/app
WORKDIR /app

# copy package.json, package-lock.json and install app dependencies
COPY package*.json /app/
RUN npm ci

# bundle app
COPY . /app/
RUN npm run build

# reinstall so only modules needed for production are moved to final container
# npm ci will remove the node_modules folder before runnning
RUN npm ci --production

# ----------------------------------
FROM node:13-alpine

WORKDIR /app
COPY --from=builder /app/package*.json /app/
COPY --from=builder /app/node_modules /app/node_modules
COPY --from=builder /app/dist /app/dist

EXPOSE 9000
CMD [ "npm", "start" ]
