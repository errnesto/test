import React, { useRef, useMemo } from 'react'
import { scaleLinear } from 'd3-scale'
import { line as lineGenerator, curveMonotoneX } from 'd3-shape'
import { forceSimulation, forceX, forceY } from 'd3-force'
import { apiFetch } from '../../lib/firstNamesApi'
import { Point, Gender, History } from '../../lib/globalTypes'
import { darkMagenta, cyan } from '../../lib/colors'

// import colors from 'global_styles/colors.sass'
// import breakpoints from 'global_styles/breakpoints.sass'
// import { Point } from 'lib/globalTypes'
// import useWindowSize from 'lib/hooks/useWindowSize'
// import _ from './LineChart.module.sass'

function fixHistory(history: History): History {
  const years = [2012, 2013, 2014, 2015, 2016, 2017, 2018, 2019]
  const gender = history[0] ? history[0].gender : 'w'

  return years.map(year => {
    const historyItem = history.find(item => item.year === year)
    const count = historyItem ? historyItem.count : 0
    return { year, count, gender }
  })
}

export async function getLine(
  nameString: string,
  gender: Gender
): Promise<Point[]> {
  const response = await apiFetch('/names/over-time', { name: nameString })

  const history: History = response.usage
  // TODO: would be nice if the api could do this
  const genderSafeHistory = history.filter(d => d.gender === gender)
  const fiexedHistory = fixHistory(genderSafeHistory)
  return fiexedHistory.map(historyItem => [historyItem.year, historyItem.count])
}

export type Line = Point[]

function createNameDotsCluster(count: number): Point[] {
  const children: any[] = Array.from(Array(count).keys()).map(() => ({}))

  const simulation: any = forceSimulation(children)
    .stop()
    .force('x', forceX(0).strength(1.3))
    .force('y', forceY(0).strength(1.3))
    .tick(1)

  return simulation.nodes().map((node: any) => [node.x, node.y])
}

function getMin(array: number[]): number {
  return array.reduce((min, next) => {
    if (next < min) return next
    return min
  }, Number.MAX_VALUE)
}

function getMax(array: number[]): number {
  return array.reduce((max, next) => {
    if (next > max) return next
    return max
  }, Number.MIN_VALUE)
}

interface Props {
  width: number
  height: number
  line: Line
  gender: Gender
}
function LineChart(props: Props) {
  const { width, height, line, gender } = props
  const color = gender === 'w' ? darkMagenta : cyan
  const svgLineRef = useRef(null)
  const padding = { top: 60, right: 75, bottom: 40, left: 75, legend: 20 }

  // setup scales
  const years = line.map(point => point[0])
  const minYear = getMin(years)
  const maxYear = getMax(years)
  const maxCount = getMax(line.map(point => point[1]))
  const domainPadding = 30

  const x = useMemo(
    () =>
      scaleLinear()
        .domain([minYear, maxYear])
        .range([padding.left, width - padding.right]),
    [minYear, maxYear, padding.left, padding.right, width]
  )
  const y = useMemo(
    () =>
      scaleLinear()
        .domain([0, maxCount + domainPadding])
        .range([height - padding.bottom, padding.top]),
    [maxCount, domainPadding, padding.bottom, padding.top, height]
  )

  // setup line generator
  const historyLine = lineGenerator<Point>()
    .curve(curveMonotoneX)
    .x(d => x(d[0]))
    .y(d => y(d[1]))

  const currentYearIndex = line.length - 1
  const firstValue = line[0][1]
  const currentValue = line[currentYearIndex][1]
  const lineEndPoint = { x: x(2019), y: y(currentValue) }

  const startDots = useMemo(() => createNameDotsCluster(firstValue), [
    firstValue
  ])
  const endDots = useMemo(() => createNameDotsCluster(currentValue), [
    currentValue
  ])

  const firstTextOffset = Math.sqrt(firstValue) * 2.8 + 15
  const lastTextOffset = Math.sqrt(currentValue) * 2.8 + 15

  return (
    <svg
      xmlns='http://www.w3.org/2000/svg'
      width={width * 2}
      height={height * 2}
      viewBox={`0 0 ${width} ${height}`}
    >
      <path
        d={historyLine(line) || ''}
        ref={svgLineRef}
        stroke={color}
        strokeWidth='2'
        fill='none'
      />

      <g transform={`translate(${x(2012)} ${y(firstValue)})`}>
        {startDots.map((dot, j) => (
          <circle key={j} cx={dot[0]} cy={dot[1]} r={1.5} fill={color} />
        ))}
      </g>
      <g transform={`translate(${lineEndPoint.x} ${lineEndPoint.y})`}>
        {endDots.map((dot, j) => (
          <circle key={j} cx={dot[0]} cy={dot[1]} r={1.5} fill={color} />
        ))}
      </g>

      {/* <text
        // className={_.fadeText}
        x={x(2012) - 15}
        y={y(firstValue) + firstTextOffset}
        textAnchor='middle'
      >
        {firstValue}
      </text>
      <text
        x={lineEndPoint.x + 15}
        y={lineEndPoint.y + lastTextOffset}
        textAnchor='middle'
      >
        {currentValue}
      </text> */}

      <line
        x1={x(2012) - 30}
        y1={y(0)}
        x2={x(2019) + 30}
        y2={y(0)}
        stroke={'darkGrey'}
        strokeWidth='1'
      />

      {/* {years.map(year => {
        const xPos = x(year)
        const yPos = height - 20
        const rotate = 45
        const textAnchor = 'start' // windowWidth < breakpoints.small ? 'start' : 'middle'
        return (
          <text
            // className={_.legend}
            key={year}
            fill={'grey'}
            x={xPos}
            y={yPos}
            transform={`rotate(${rotate} ${xPos} ${yPos})`}
            textAnchor={textAnchor}
          >
            {year}
          </text>
        )
      })} */}
    </svg>
  )
}

export default LineChart
