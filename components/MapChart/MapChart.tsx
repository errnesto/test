import React, { useState, useEffect } from 'react'
import { Gender, Name, District } from '../../lib/globalTypes'
import { apiFetch } from '../../lib/firstNamesApi'
import {
  getDistrictColor,
  getDistrictRadius,
  getRadiusScale,
  getColorScale,
  getPercentageFromDistrict
} from '../../lib/dotgridHelpers'
import DotgridMap from '../DotgridMap/DotgridMap'
// import _ from './PersonalNameInput.module.sass'

export async function getRegionalData(nameString: string, gender: Gender) {
  const regionalData = await apiFetch('/v2/by-district/multiple-years', {
    name: nameString,
    gender: gender,
    years: [2012, 2013, 2014, 2015, 2016, 2017, 2018, 2019]
  })

  const districts: {
    [key: string]: District
  } = regionalData.reduce((districts: { [key: string]: District }, res) => {
    res.districts.forEach((district: District) => {
      if (districts[district.district] === undefined) {
        districts[district.district] = {
          district: district.district,
          count: 0,
          total: 0
        }
      }
      districts[district.district].count += district.count
      districts[district.district].total += district.total
    })

    return districts
  }, {})

  return Object.values(districts)
}

interface Props {
  regionalData: District[]
  gender: Gender
}
function MapChart(props: Props) {
  const { regionalData, gender } = props
  // const name: Name = { name: nameString, gender: gender }

  const domain = regionalData.map(distrcit =>
    getPercentageFromDistrict(distrcit)
  )
  const radiusScale = getRadiusScale(domain)
  const colorScale = getColorScale(domain, gender)
  const totalCount = regionalData.reduce(
    (sum: number, d: District) => sum + d.count,
    0
  )

  function getColor(borough: string) {
    return getDistrictColor(regionalData, borough, colorScale)
  }

  function getRadius(borough: string) {
    return getDistrictRadius(regionalData, borough, radiusScale)
  }

  // {/* <div>{name && <NameRankingRow name={name} count={totalCount} />}</div> */}
  return <DotgridMap getColor={getColor} getRadius={getRadius} />
}

export default MapChart
