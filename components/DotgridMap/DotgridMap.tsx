import React from 'react'
import { Point } from '../../lib/globalTypes'
// import { makeGridMapPoints } from './gridFromGeoJSON'
// import _ from './DotgridMap.module.sass'

// the points array was generated with the following function:
// > const points = makeGridMapPoints(500, 400, 8)
// but so we don't have to include d3-geo and d3-polygon and
// don't recalucalte the points all the time we just
// store and load it from that file:
const points = require('./points.json') as { dot: Point; borough: string }[]

interface Props {
  getColor: (borough: string) => string
  getRadius: (borough: string) => number
}
function DotgridMap(props: Props) {
  const { getColor, getRadius } = props

  return (
    <svg
      xmlns="http://www.w3.org/2000/svg"
      width={1200}
      height={630}
      viewBox='0 -20 500 440'>
      <g>
        {points.map((point, i) => (
          <circle
            key={i}
            cx={point.dot[0]}
            cy={point.dot[1]}
            r={getRadius(point.borough)}
            fill={getColor(point.borough)}
          />
        ))}
      </g>
    </svg>
  )
}

export default React.memo(DotgridMap)
