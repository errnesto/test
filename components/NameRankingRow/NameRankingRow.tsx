import React from 'react'
import * as colors from '../../lib/colors'
import { Name } from '../../lib/globalTypes'
import DotSwarm from './DotSwarm'
// import _ from './NameRankingRow.module.sass'

interface Props {
  name: Name
  count: number
  position?: Number
}
function NameRankingRow(props: Props) {
  const { name, count, position } = props

  return (
    <li>
      <DotSwarm
        width={100}
        height={100}
        count={count}
        radius={1.5}
        color={name.gender === 'w' ? colors.darkMagenta : colors.cyan}
      />
      <p>
        {position && <span>Platz {position}</span>}
        <span>{name.name}</span>
        <span>{count} mal im Jahr 2019</span>
      </p>
    </li>
  )
}

export default React.memo(NameRankingRow)
