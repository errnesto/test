const qs = require('querystring')
const { URL_PREFIX = '' } = process.env

const url = require('url')

export function isFacebookBot(uaString) {
  return /^(Facebot|facebookexternalhit)/.test(uaString)
}

export function isTwitterBot(uaString) {
  return /^Twitterbot/.test(uaString)
}

export function fixUrlPrefix(urlPrefix = '') {
  let p = urlPrefix.replace(/(\/)+$/, '').replace(/^(\/)+/, '')
  return p.length === 0 ? p : '/' + p
}

export function normalizeHost(host) {
  return host.replace(/(\/)+$/, '')
}

export function fetchLocation(locations, type, idx) {
  if (
    idx == null ||
    (type !== 'city' && type !== 'state' && type !== 'country')
  )
    return null

  const array =
    type === 'state'
      ? locations.sortedStates
      : type === 'country'
      ? locations.sortedCountries
      : locations.cities.features
  const item = array[idx]
  return type === 'city' && item ? item.properties : item
}

/**
 * Returns the url for a route as it is mounted on a running server
 * @param  {String}     protocol http / https
 * @param  {KoaContext} context
 * @param  {String}     route
 * @return {String}
 */
export function routeUrl(protocol, context, route = '') {
  return new url.URL(
    `.${fixUrlPrefix(URL_PREFIX)}/${route}?${qs.stringify(context.query)}`,
    `${protocol}://${context.headers.host}`
  ).href
}

/**
 * Returns an absolute URL to an image, given the request headers
 * and a location from our dataset.
 * @return String
 */
export function imageUrl(protocol, headers, type, name, gender) {
  return new url.URL(
    `.${fixUrlPrefix(URL_PREFIX)}/thumbs/${type}.png?name=${name}&gender=${gender}`,
    `${protocol}://${headers.host}`
  ).href
}

export function isRelativeUrl(url) {
  return !/^(\w)*:\/\//.test(url)
}

export function normalizeRelativeUrl(url) {
  return url.replace(/^\//, '').replace(/(\.\/|\.\.\/)/g, '')
}
