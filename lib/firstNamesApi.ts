/* global fetch */
import fetch from 'isomorphic-unfetch'
import { reduce } from 'lodash'

type Value = string | number

interface Params {
  [key: string]: Value | Value[]
}
export async function apiFetch(endpoint: string, params: Params) {
  const paramsString = paramsToString(params)
  const res = await fetch(
    `${process.env.FIRST_NAMES_API_ENDPOINT}${endpoint}?${paramsString}`,
    {
      method: 'GET',
      headers: {
        'X-Api-Key': process.env.FIRST_NAMES_API_KEY
      }
    }
  )

  const json = await res.json()
  return json
}

/**
 * construct query string from params object
 */
function paramsToString(params: Params) {
  return reduce(
    params,
    (string, value, key) => {
      if (string !== '') string += '&'
      if (Array.isArray(value)) {
        string += parseArray(key, value)
      } else {
        string += `${key}=${value}`
      }

      return string
    },
    ''
  )
}

function parseArray(key: string, value: Array<Value>) {
  return value.reduce((string, val) => {
    if (string !== '') string += '&'
    string += `${key}=${val}`
    return string
  }, '')
}
