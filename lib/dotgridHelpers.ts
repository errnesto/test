// import Chroma from 'chroma-js'
import * as colors from './colors'
import { Gender, District } from './globalTypes'

const scaleCluster = require('d3-scale-cluster')

// colors were created with Chroma js like this:
// const girlsColors =  Chroma.scale([colors.beige, colors.darkMagenta]).padding([0.5, 0]).colors(5)
// const boysColors = Chroma.scale([colors.beige, colors.cyan]).padding([0.5, 0]).colors(5)

const girlsColors = ['#cf8dbc', '#ca79b6', '#c665b0', '#c250ab', '#be3ca5']
const boysColors = ['#70bb9f', '#54b292', '#38a986', '#1ca079', '#00976c']

export function getPercentageFromDistrict(district: District) {
  // round percentage so close values fall together
  // PRESICION of 200 means we have an accuracy of half a percent
  // if we make that value bigger we become more accurate
  //
  // Jenks will not solve this because if we have values like
  // [0.5%, 0.45%, 0.44%, 0.4%] they might end up in different
  // buckets even though our samples are way to small for these
  // differences to have any meaning.

  const PRESICION = 300

  // just for saftey add 1 so we dont have to think about what happens at 0
  return Math.round((district.count / district.total) * PRESICION) + 1
}

export function getRadiusScale(domain: number[]): (val: number) => number {
  if (domain.length === 0) return () => 1

  return scaleCluster()
    .domain(domain)
    .range([5, 8, 11, 14, 17])
}

export function getColorScale(
  domain: number[],
  gender: Gender
): (val: number) => string {
  if (domain.length === 0) return () => colors.lightGrey

  return scaleCluster()
    .domain(domain)
    .range(gender === 'w' ? girlsColors : boysColors)
}

export function getDistrictRadius(
  districts: District[],
  districtName: string,
  scale: (val: number) => number
): number {
  const district = districts.find(
    district => district.district === districtName
  )

  if (district === undefined) return 1.5

  const percentage = getPercentageFromDistrict(district)
  const size = scale(percentage)
  // take the sqare root so we set the area of the circle
  // which is what visually defines the "amount" of it
  return Math.sqrt(size)
}

export function getDistrictColor(
  districts: District[],
  districtName: string,
  scale: (val: number) => string
): string {
  const district = districts.find(
    district => district.district === districtName
  )

  if (district === undefined) return colors.lightGrey

  const percentage = getPercentageFromDistrict(district)
  return scale(percentage)
}
