export type Point = [number, number]

export type Gender = 'w' | 'm' | 'd'
export interface Name { name: string, gender: Gender }
export interface RankingItem { name: Name, count: number }
export type Ranking = RankingItem[]

export interface District {
  district: string
  count: number,
  total: number
}

export interface HistoryItem {
  year: number,
  count: number,
  gender: Gender
}

export type History = HistoryItem[]