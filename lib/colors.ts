// rbb colors
export const red = '#e31818'

// TODO: there are different greys in different places
// find out which ones are the right ones…
export const backgroundGrey = '#eaeaea'
export const darkGrey = '#585856'
export const grey = '#868686'
export const lightGrey = '#D5D5D5'

// on air design manual datavis colors
// - general
export const orange = '#fa960f'
export const darkOrange = '#EF7401'
export const honey = '#fac200'
export const yellow = '#ffdc37'

export const darkGreen = '#1e5a3a'
export const green = '#699b32'
export const lightGreen = '#c3d719'

export const darkBlue = '#3b389d'
export const blue = '#2887f0'
export const lightBlue = '#3fbaed'

export const darkMagenta = '#be3ca5'
export const magenta = '#f03ca0'

export const darkBeige = '#aaa493'
export const beige = '#dfded2'

// - water on maps
export const darkWaterBlue = '#266ca3'
export const lightWaterBlue = '#86c6ed'

// - political parties

export const spd = '#e31818'
export const linke = '#be3ca5'
export const cdu = '#000'
export const afd = '#2887f0'
export const fdp = '#ffdc37'
export const gruene = '#699b32'

// this used to be a proposed color for the styleguide it
// never made it but we use it in some places anyway :-)
export const bordaux = '#821423'
export const cyan = '#00976C'
